# T12 Lituoklis

A project that aims to create a soldering station that's:
*  Cheaper than good-quality commercial stations;
*  Easy to assemble at home;
*  Uses high-quality soldering tips.

List of materials:
*  Hakko T12 soldering iron tips;
*  Arduino Nano;
*  OLED display(or any I2C compatible screen);
*  3 momentary switches;
*  LM358 op-amp(SOP8 package);
*  2*1N5819 Schotky diodes;
*  IRF540N N-channel MOSFET;
*  2SC3356-T1B-A NPN transistor(SMD package);
*  2.2nF electrolytic capacitor;
*  DC-DC step-down/buck module;
*  1*10kOhm resistor(THT package);
*  6*1kOhm resistors(THT package);
*  2*100kOhm resistors(THT package);

This repository is storaging the following:
*  Arduino source file(.ino);
*  Schematic files;
*  PCB design files(gerber files);