#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <FastPID.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#if (SSD1306_LCDHEIGHT != 32)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

///////Predefined pins

int MOSFET_PIN = 3; //pin to give PWM signal to the MOSFET
int incTemp = 5; //pin for increasing set temperature
int rstTemp = 6; //pin for setting set temperature to 0
int decTemp = 7; //pin for decreasing set temperature
int temp_sense = A0; //pin to get input from opamp

///////PID variables

float Kp=7.98, Ki=0.055, Kd=0.86, Hz=10;
//float Kp=1.00, Ki=0, Kd=0, Hz=10;
int output_bits = 8;
bool output_signed = false;
FastPID myPID(Kp, Ki, Kd, Hz, output_bits, output_signed);

///////Editable variables

float min_temp = 0; //min temp of the soldering iron
float max_temp = 500; //max temp of the soldering iron
float Delay = 300;//ms between loop cycles
int setpoint = 0; //initial on boot set temp value;

///////Other variables

float temperature_read;
int temp_to_print = 0;
int i = 0;
int i_prev = 0;
bool set_temp_made = false;

///////ADC sampling variables
double CurrentTemp;
#define ADC_MULTISAMPLING 5
#define ADC_MULTISAMPLING_SAMPLES (1 << ADC_MULTISAMPLING)
///////

void setup() {
  pinMode(MOSFET_PIN, OUTPUT);
  pinMode(incTemp, INPUT);
  pinMode(rstTemp, INPUT);
  pinMode(decTemp, INPUT);
  pinMode(temp_sense, INPUT);
  Serial.begin(9600);

  digitalWrite(MOSFET_PIN, HIGH); //turn off the mosfet with pulling the BJT to high
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); //initialise display with I2C address 0x3c(for 128x32 screen)
  delay(200);
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Hellcats");
  display.setTextSize(1);
  display.println("Hello world @COVID19");
  display.println("nightmare nightmare nightmare nightmare nightmare ");
  display.display();

  delay(3000);

}

void loop() {
  digitalWrite(MOSFET_PIN, HIGH); //turn off MOSFET so we can read thermocouple
  delayMicroseconds(500);
  temperature_read = read_temperature();
  Serial.print("temperature_read=");
  Serial.print(temperature_read);

  //PID part
  //uint32_t before, after;
  //before = micros();
  //uint8_t output = myPID.step(setpoint, temperature_read);
  //after = micros();
  //Serial.print("  uint8 output=");
  //Serial.print(output);

  if(temperature_read >= setpoint)
    digitalWrite(MOSFET_PIN, HIGH);
  if(temperature_read < setpoint)
    digitalWrite(MOSFET_PIN, LOW);
  //analogWrite(MOSFET_PIN, 255-output);

  temp_to_print = mapTemp(temperature_read);

  display.clearDisplay(); 
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("Get: ");
  display.println(temp_to_print);
  display.print("Set: ");
  display.println(setpoint);
  display.display();

  if(digitalRead(incTemp) == HIGH && setpoint < max_temp) {
    digitalWrite(MOSFET_PIN, HIGH);
    setpoint+=10;
  }

  if(digitalRead(decTemp) == HIGH && setpoint > min_temp){
    digitalWrite(MOSFET_PIN, LOW);
    setpoint-=10;
  }
    
  if(digitalRead(rstTemp) == HIGH){
    digitalWrite(MOSFET_PIN, HIGH);
    setpoint = 0;
  }

  delay(100);
}

//Temperature remapping function, corrects the displayed temp within 5 degrees
int mapTemp(int temp)
{
  while (i < 800)
  {
     if(i_prev < temp && temp < i)
     {
        return i;
        i=800;
     }
     i_prev=i;
     i=i+5 ;    
  }
  i = 0;
}

//Reads the temperature
float read_temperature() {
  int adc = 0;
  for(int i = 0; i < ADC_MULTISAMPLING_SAMPLES; ++i)
    adc += analogRead(temp_sense);
    adc = adc >> ADC_MULTISAMPLING;
    Serial.print("  AnalogRead=");
    Serial.println(analogRead(temp_sense));
    double temp = 1.8025*adc;

    CurrentTemp += (temp-CurrentTemp) * 0.05;
    return(CurrentTemp);
}
